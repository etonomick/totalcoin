package com.etonomick.totalcoin;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewsCategoryFragment extends Fragment {

    String url;
    ArrayList<HashMap> news;
    ProgressBar progressBar;
    GridView gridView;
    BaseAdapter baseAdapter;
    RequestQueue requestQueue;
    LruCache<String, Bitmap> lruCache;

    public NewsCategoryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View newsCategoryView = inflater.inflate(R.layout.fragment_news_category, container, false);
        progressBar = newsCategoryView.findViewById(R.id.progress_bar_news_category);

        news = new ArrayList<>();

        lruCache = new LruCache<>(1024);

        gridView = newsCategoryView.findViewById(R.id.grid_view_news);
        baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return news.size();
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View card = getLayoutInflater().inflate(R.layout.news_card, viewGroup, false);

                final HashMap entry = news.get(i);

                TextView titleTextView = card.findViewById(R.id.text_view_title);
                titleTextView.setText(entry.get("title").toString());

                final ImageView imageView = card.findViewById(R.id.image_view_cover);
                if (lruCache.get(entry.get("image").toString()) != null) {
                    imageView.setImageBitmap(lruCache.get(entry.get("image").toString()));
                }
                else {
                    requestQueue.add(new ImageRequest("http://totalcoin.ru/" + entry.get("image").toString(), new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            lruCache.put(entry.get("image").toString(), response);
                            imageView.setImageBitmap(response);
                        }
                    }, 0, 0, null, null, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }));
                }

                return card;
            }
        };
        gridView.setAdapter(baseAdapter);

        requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(new StringRequest(Request.Method.GET, getActivity().getString(R.string.news_url) + "/" + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.trim().replaceAll("[\n\r]", "");
                Matcher matcher = Pattern.compile("<div class=\"img_block smart_img_outer\">(.*?)</div>").matcher(response);
                while (matcher.find()) {
                    Matcher titleMatcher = Pattern.compile("alt=\"(.*?)\"").matcher(matcher.group(1));
                    Matcher imageMatcher = Pattern.compile("src=\"(.*?)\"").matcher(matcher.group(1));
                    while (titleMatcher.find() && imageMatcher.find()) {
                        HashMap entry = new HashMap();
                        entry.put("title", titleMatcher.group(1));
                        entry.put("image", imageMatcher.group(1));
                        news.add(entry);
                    }
                }
                progressBar.setVisibility(View.INVISIBLE);
                baseAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }));

        return newsCategoryView;
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        url = args.getString("url");
    }
}
