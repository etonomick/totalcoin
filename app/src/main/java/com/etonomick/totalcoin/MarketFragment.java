package com.etonomick.totalcoin;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarketFragment extends Fragment {

    RequestQueue requestQueue;
    ArrayList<HashMap> marketData;
    ProgressBar progressBar;
    ListView marketListView;
    BaseAdapter baseAdapter;
    LruCache <String, Bitmap> lruCache;

    public MarketFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_market, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_sort_alphabet:
                sortBy("name");
                break;
            case R.id.item_sort_price:
                sortBy("price");
                break;
            case R.id.item_volume:
                sortBy("volume");
                break;
            case R.id.item_complexity:
                sortBy("complexity");
                break;
            case R.id.item_capitalisation:
                sortBy("capitalisation");
                break;
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortBy(final String key) {
        Collections.sort(marketData, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap e1, HashMap e2) {
                String value1 = e1.get(key) != null ? e1.get(key).toString() : "0";
                String value2 = e2.get(key) != null ? e2.get(key).toString() : "0";
                return value1.compareTo(value2);
            }
        });
        baseAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View marketView = inflater.inflate(R.layout.fragment_market, container, false);

        progressBar = marketView.findViewById(R.id.progress_bar_market);

        marketData = new ArrayList<>();
        lruCache = new LruCache<>(1024);

        marketListView = marketView.findViewById(R.id.list_view_market);
        baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return marketData.size();
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View marketRow = getLayoutInflater().inflate(R.layout.market_row, viewGroup, false);

                final HashMap<String, String> entry = marketData.get(i);

                TextView currencyNameTextView = marketRow.findViewById(R.id.text_view_currency_name);
                currencyNameTextView.setText(entry.get("name"));

                final ImageView currencyIconImageView = marketRow.findViewById(R.id.image_view_currency_icon);

                if (lruCache.get(entry.get("image")) != null) {
                    currencyIconImageView.setImageBitmap(lruCache.get(entry.get("image")));
                }
                else {
                    requestQueue.add(new ImageRequest("http://totalcoin.ru" + entry.get("image"), new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            lruCache.put(entry.get("image"), response);
                            currencyIconImageView.setImageBitmap(response);
                        }
                    }, 0, 0, null, null, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }));
                }

                TextView capitalisationTextView = marketRow.findViewById(R.id.text_view_capitalisation);
                capitalisationTextView.setText(String.format(Locale.getDefault(), "Капитализация: %s", entry.get("capitalisation")));

                TextView priceTextView = marketRow.findViewById(R.id.text_view_price);
                priceTextView.setText(String.format(Locale.getDefault(), "Цена: %s", entry.get("price")));

                TextView volumeTextView = marketRow.findViewById(R.id.text_view_volume);
                volumeTextView.setText(String.format(Locale.getDefault(), "Объем: %s", entry.get("volume")));

                TextView complexityTextView = marketRow.findViewById(R.id.text_view_complexity);
                complexityTextView.setText(String.format(Locale.getDefault(), "Сложность: %s", entry.get("complexity")));

                return marketRow;
            }
        };
        marketListView.setAdapter(baseAdapter);

        requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(new StringRequest(Request.Method.GET, getActivity().getString(R.string.market_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.trim().replaceAll("[\r\n]", "");
                final String[] keys = {"capitalisation", "price", "volume", "complexity"};
                Matcher marketEntryMatcher = Pattern.compile("<tr data-key=\".*?\">(.*?)</tr>").matcher(response);
                while (marketEntryMatcher.find()) {
                    String find = marketEntryMatcher.group(1);
                    Matcher imageMatcher = Pattern.compile("<img src='(.*?)' alt='.*?' border=0/>").matcher(find);
                    Matcher nameMatcher = Pattern.compile("<span class='text_outer'><span>(.*?)</span>").matcher(find);
                    Matcher valuesMatcher = Pattern.compile("<td data-sort=\"(.*?)\">").matcher(find);
                    while (imageMatcher.find() && nameMatcher.find()) {
                        HashMap entry = new HashMap();
                        entry.put("image", imageMatcher.group(1));
                        entry.put("name", nameMatcher.group(1));
                        marketData.add(entry);
                        int keyIndex = 0;
                        while (valuesMatcher.find()) {
                            entry.put(keys[keyIndex], valuesMatcher.group(1));
                            keyIndex++;
                        }
                    }
                }
                progressBar.setVisibility(View.INVISIBLE);
                baseAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }));

        return marketView;
    }

}
