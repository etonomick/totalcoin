package com.etonomick.totalcoin;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewsFragment extends Fragment {

    ProgressBar progressBar;
    ViewPager viewPager;
    PagerTabStrip pagerTabStrip;
    FragmentPagerAdapter fragmentPagerAdapter;
    RequestQueue requestQueue;
    HashMap<String, String> categories;

    public NewsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View newsView = inflater.inflate(R.layout.fragment_news, container, false);

        progressBar = newsView.findViewById(R.id.progress_bar_news);

        viewPager = newsView.findViewById(R.id.view_pager);
        pagerTabStrip = newsView.findViewById(R.id.pager_tab_strip);
        pagerTabStrip.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        pagerTabStrip.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        pagerTabStrip.setTabIndicatorColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
        pagerTabStrip.setVisibility(View.INVISIBLE);

        categories = new HashMap<>();

        fragmentPagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                NewsCategoryFragment newsCategoryFragment = new NewsCategoryFragment();
                Bundle bundle = new Bundle();
                bundle.putString("url", categories.get(categories.keySet().toArray()[position]));
                newsCategoryFragment.setArguments(bundle);
                return newsCategoryFragment;
            }

            @Override
            public int getCount() {
                return categories.keySet().size();
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return categories.keySet().toArray()[position].toString().toUpperCase();
            }
        };
        viewPager.setAdapter(fragmentPagerAdapter);

        requestQueue = Volley.newRequestQueue(getActivity());
        getCategories();

        return newsView;
    }

    private void getCategories() {
        progressBar.setVisibility(View.VISIBLE);
        categories = new HashMap<>();
        requestQueue.add(new StringRequest(Request.Method.GET, getActivity().getString(R.string.news_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.trim().replaceAll("[\r\n]", "");
                Matcher categoryMatcher = Pattern.compile("<ul class=\"categories_tabs_nav mb50\">(.*?)</ul>").matcher(response);
                while (categoryMatcher.find()) {
                    Matcher categoryNameMatcher = Pattern.compile("<a href=\"/news/.*?\">(.*?)</a>").matcher(categoryMatcher.group());
                    Matcher categoryUrlMatcher = Pattern.compile("<a href=\"/news/(.*?)\">.*?</a>").matcher(categoryMatcher.group());
                    while (categoryNameMatcher.find() && categoryUrlMatcher.find()) {
                        categories.put(categoryNameMatcher.group(1), categoryUrlMatcher.group(1));
                    }
                }
                progressBar.setVisibility(View.INVISIBLE);
                pagerTabStrip.setVisibility(View.VISIBLE);
                fragmentPagerAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);

                Snackbar.make(getView(), getActivity().getString(R.string.error_getting_news), Snackbar.LENGTH_INDEFINITE).setAction(getActivity().getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getCategories();
                    }
                }).show();
            }
        }));
    }

}
